#!/bin/bash

## 1TB
disk_size=454
half_size=$((disk_size / 2 ))
four_percent=18
today=randomdata

cd /mnt/tempvolume
if [ -e $today ]
then
    echo 'Folder exists'
else
    mkdir $today ;
fi
cd $today

cmd="seq 1 $four_percent"

## Now Create 1 GB files til 'half full'

for i in $(eval $cmd);
do
    echo "Generating file $i.file"
    dd if=/dev/urandom of=$i.file bs=1048576 count=1024 ;
done;

cd /root/
if [ -e $today ]
then
    echo 'Folder exists'
else
    mkdir $today ;
fi
cd $today

## 60 GB --
disk_size=30
half_size=$((disk_size / 2 ))
four_percent=1
today=randomdata

cmd="seq 1 $four_percent"

## Now Create 1 GB files til 'half full'

for i in $(eval $cmd);
do
    echo "Generating file $i.file"
    dd if=/dev/urandom of=$i.file bs=1048576 count=1024 ;
done;

cd /root/
if [ -e $today ]
then
    echo 'Folder exists'
else
    mkdir $today ;
fi
cd $today

